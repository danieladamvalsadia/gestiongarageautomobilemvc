<?php
	
	require_once('modele/modele.php');
	require_once('vue/vue.php');
	require_once('controleur/controleur.php');
	require_once('site.php');
	require_once('connect.php');
	
	function getConnect(){
		$connexion=new PDO('mysql:host='.SERVEUR.';dbname='.BDD,USER,PASSWORD);
		$connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$connexion->query('SET NAMES UTF8');
		return $connexion;
	}

//GESTION DES EMPLOYES
	function creerEmploye($nomEmp,$loginEmp, $mdpEmp, $catEmp) {
		$connexion=getConnect();
		$requete="INSERT INTO employes VALUES('$nomEmp', '$loginEmp', '$mdpEmp', '$catEmp')";	
		$resultat=$connexion->query($requete);  
		$resultat->closeCursor();
	}
	function modifierEmploye($loginEmp, $mdpEmp, $catEmp,$nomEmp) {
		$connexion=getConnect();
		$requete="UPDATE employes SET login='$loginEmp',mdp='$mdpEmp',categorie='$catEmp' WHERE nomEmp='$nomEmp'";
		$resultat=$connexion->query($requete);  
		$resultat->closeCursor();
	
	}
	function rechercherUnEmploye($nomEmp) {
		$connexion=getConnect();
		$requete="SELECT * FROM employes WHERE nomEmp='$nomEmp'"; 
		$resultat=$connexion->query($requete);  	
		$resultat->setFetchMode(PDO::FETCH_OBJ);
		$employe=$resultat->fetch();
		$resultat->closeCursor();
		return $employe;
	}
    
    function rechercherLoginEmploye($login) {
		$connexion=getConnect();
		$requete="SELECT * FROM employes WHERE login='$login'"; 
		$resultat=$connexion->query($requete);  	
		$resultat->setFetchMode(PDO::FETCH_OBJ);
		$employe=$resultat->fetch();
		$resultat->closeCursor();
		return $employe;
	}
    function getLoginEmploye($login) {
		$connexion=getConnect();
		$requete="SELECT * FROM employes WHERE login='$login'"; 
		$resultat=$connexion->query($requete);  	
		$resultat->setFetchMode(PDO::FETCH_OBJ);
		$nbLigne=$resultat->rowcount();
		$resultat->closeCursor();
		return $nbLigne;
	}
	function rechercherLesEmployes() {
		$connexion=getConnect();
		$requete="SELECT*FROM employes WHERE 1";
		$resultat=$connexion->query($requete);  
		$resultat->setFetchMode(PDO::FETCH_OBJ);	
		$employes=$resultat->fetchall();
		$resultat->closeCursor();
		return $employes;
	}
	function supprimerEmploye($nomEmp) {
		$connexion=getConnect();
		$requete="DELETE FROM employes WHERE nomEmp='$nomEmp'";
		$resultat=$connexion->query($requete); 
		$resultat->closeCursor();
	}

//GESTION DES INTERVENTIONS
    function creerIntervention($nomTI,$montant,$listePiece) {
        $connexion=getConnect(); 
        $requete="INSERT INTO typeintervention VALUES('$nomTI', '$montant', '$listePiece')"; 
        $resultat=$connexion->query($requete);  
		$resultat->closeCursor();
    }
    function modifierIntervention($montant, $listePiece, $nomTI) {
        $connexion=getConnect();
		$requete="UPDATE typeintervention SET montant='$montant',listePiece='$listePiece' WHERE nomTI='$nomTI'";
		$resultat=$connexion->query($requete);  
		$resultat->closeCursor();
    }
	function rechercherUneIntervention($nomTI) {
        $connexion=getConnect();
		$requete="SELECT * FROM typeintervention WHERE nomTI='$nomTI'"; 
		$resultat=$connexion->query($requete);  	
		$resultat->setFetchMode(PDO::FETCH_OBJ);
		$intervention=$resultat->fetch();
		$resultat->closeCursor();
		return $intervention;
    }
	function rechercherLesInterventions() {
		$connexion=getConnect();
		$requete="SELECT*FROM typeintervention WHERE 1";
		$resultat=$connexion->query($requete);  
		$resultat->setFetchMode(PDO::FETCH_OBJ);	
		$employes=$resultat->fetchall();
		$resultat->closeCursor();
		return $employes;
	}	
	function supprimerIntervention($nomTI) {
		$connexion=getConnect();
		$requete="DELETE FROM typeintervention WHERE nomTI='$nomTI'";
		$resultat=$connexion->query($requete); 
		$resultat->closeCursor();
	}

//GESTION DES CONNEXION
    function insertionConnexion($nomEmp,$mdp,$catEmp,$login) {
        $connexion=getConnect(); 
        $requete="INSERT INTO user VALUES('$nomEmp', '$login','$mdp', '$catEmp')"; 
        $resultat=$connexion->query($requete);  
		$resultat->closeCursor();
    }
    function tantativeConnexion($login, $mdp) {
        $connexion=getConnect();
		$requete="SELECT * FROM employes WHERE login='$login' AND mdp='$mdp'"; 
        $resultat=$connexion->query($requete);  	
		$resultat->setFetchMode(PDO::FETCH_OBJ);
		$tantativeConnexion=$resultat->fetch();
		$resultat->closeCursor();
		return $tantativeConnexion;
    }
    function employePresent() {
        $connexion=getConnect();
        $requete="SELECT * FROM user";
        $resultat=$connexion->query($requete);  	
		$resultat->setFetchMode(PDO::FETCH_OBJ);
		$userPresent=$resultat->fetchall();
		$resultat->closeCursor();
		return $userPresent;
    }
    function employePresent2($login,$mdp) {
        $connexion=getConnect();
        $requete="SELECT * FROM user WHERE login='$login' AND mdp='$mdp'";
        $resultat=$connexion->query($requete);  	
		$resultat->setFetchMode(PDO::FETCH_OBJ);
		$userPresent=$resultat->fetch();
		$resultat->closeCursor();
		return $userPresent;
    }
    function deconnexion(){
        $connexion=getConnect();
		$requete="DELETE FROM user WHERE 1";
		$resultat=$connexion->query($requete); 
		$resultat->closeCursor();
    }



//fct de recherche
function rechercherClient(){
	$connexion=getConnect();
	$requete="SELECT * FROM client"; 
	$resultat=$connexion->query($requete);
	$resultat->setFetchMode(PDO::FETCH_OBJ);
	$client=$resultat->fetchAll();
	$resultat->closeCursor();
	return $client;
}

function rechercherUnClient($idClient){
	$connexion=getConnect();
	$requete="SELECT * FROM client WHERE idClient='$idClient'"; 
	$resultat=$connexion->query($requete);
	$resultat->setFetchMode(PDO::FETCH_OBJ);
	$client=$resultat->fetch();
	$resultat->closeCursor();
	return $client;

}

//RECHERCHE  ID CLIENT
function rechercherIDClient($nom,$prenom,$date){
	$connexion=getConnect();
	$requete="SELECT idClient,nom, prenom FROM client WHERE nom='$nom' AND prenom='$prenom' AND date='$date'"; 
	$resultat=$connexion->query($requete);
	$resultat->setFetchMode(PDO::FETCH_OBJ);
	$client=$resultat->fetch();
	$resultat->closeCursor();
	return $client;
}

//ajouter

function ajouterClient ($nom,$prenom,$date,$adresse,$num,$mail,$montantMax){
	$connexion=getConnect();
	$requete="INSERT INTO client VALUES('0','$nom','$prenom','$date','$adresse','$num','$mail','$montantMax')"; 
	$resultat=$connexion->query($requete);  
	$resultat->closeCursor();
}

//supprimer
function supprimerClient($idCli){
	$connexion=getConnect();
	$requete="DELETE FROM client WHERE (idClient=$idCli)"; 
	$resultat=$connexion->query($requete);  
	$resultat->closeCursor();
}

    //modifier
	function modifierClient($nom,$prenom,$date,$adresse,$num,$mail,$montantMax,$idClient) {
		$connexion=getConnect();
		$requete="UPDATE client SET nom='$nom',prenom='$prenom', date='$date',adresse='$adresse' , num='$num' , mail='$mail' , montantMax='$montantMax' WHERE idClient='$idClient'";
		$resultat=$connexion->query($requete);  
		$resultat->closeCursor();
	}

		//SYNTHESE CLIENT
		//liste d'interventions et leur montant


	function rechercherInterventionEtMontant($idClient){
		$connexion=getConnect();
		$requete="SELECT * FROM (intervention NATURAL JOIN typeintervention) WHERE (idClient='$idClient' AND dateIntervention<=NOW())";  //on sélectionne les interventions réalisées sans celles à venir
		$resultat=$connexion->query($requete);
		$resultat->setFetchMode(PDO::FETCH_OBJ);
		$intervention=$resultat->fetchAll();
		$resultat->closeCursor();
		return $intervention;
	}
	

//GESTION FINANCIERE

function rechercherinterventionsDifférées($idClient){ //renvoit toutes les interventions en état différé et dont la date/heure est antérieure à cet instant
	$connexion=getConnect();
	$requete="SELECT * FROM (intervention NATURAL JOIN typeintervention) WHERE (idClient='$idClient' AND dateIntervention<=NOW() AND etat='différé')";  //on sélectionne les interventions réalisées sans celles à venir
	$resultat=$connexion->query($requete);
	$resultat->setFetchMode(PDO::FETCH_OBJ);
	$interventionsDifférées=$resultat->fetchAll();
	$resultat->closeCursor();
	return $interventionsDifférées;
}

function rechercherinterventionAPayer($idClient){ //Une seule intervention à payer > on concidère que l'agent fait bien son boulot et ne laisse pas les clients partir sans payer
	$connexion=getConnect();
	$requete="SELECT * FROM (intervention NATURAL JOIN typeintervention) WHERE (idClient='$idClient' AND dateIntervention<=NOW() AND etat='en attente')";  //on sélectionne les interventions réalisées en attente de payement
	$resultat=$connexion->query($requete);
	$resultat->setFetchMode(PDO::FETCH_OBJ);
	$interventionsDifférées=$resultat->fetch();
	$resultat->closeCursor();
	return $interventionsDifférées;
}
//rembourser un payement différé
function payementCaisse($code){ 
	$connexion=getConnect();
	$requete="UPDATE intervention SET etat='payé' WHERE (code='$code')"; 
	$resultat=$connexion->query($requete);  
	$resultat->closeCursor();
}

//Payer en différé
function payementDifféré($code){ 
	$connexion=getConnect();
	$requete="UPDATE intervention SET etat='différé' WHERE (code='$code')"; 
	$resultat=$connexion->query($requete);  
	$resultat->closeCursor();
}


function getMontantMax($idClient){  //recupère le montantMax du client
	$connexion=getConnect();
	$requete="SELECT montantMax FROM client WHERE idClient='$idClient'"; 
	$resultat=$connexion->query($requete);
	$resultat->setFetchMode(PDO::FETCH_OBJ);
	$montantMax=$resultat->fetch();
	$resultat->closeCursor();
	return $montantMax;
}

function getIdClient($id){  //recupère l'idClient en fct du code intervention
	$connexion=getConnect();
	$requete="SELECT idclient FROM intervention WHERE idClient='$id'"; 
	$resultat=$connexion->query($requete);
	$resultat->setFetchMode(PDO::FETCH_OBJ);
	$idClient=$resultat->fetch();
	$resultat->closeCursor();
	return $idClient;
}

function etatIntervention($code){ //recupère l'état de l'intervention correspondante
	$connexion=getConnect();
	$requete="SELECT etat FROM intervention WHERE code='$code'"; 
	$resultat=$connexion->query($requete);
	$resultat->setFetchMode(PDO::FETCH_OBJ);
	$etat=$resultat->fetch();
	$resultat->closeCursor();
	return $etat;
}

	function modifierMontantMax($montantAChanger,$idClient){ //récupère le montant à changer et l'update dans le montant client
	$connexion=getConnect();
	$requete="UPDATE client SET montantMax='$montantAChanger' WHERE (idClient='$idClient')"; 
	$resultat=$connexion->query($requete);  
	$resultat->closeCursor();
}

function getMontantIntervention($code){  //recupère le montantMax du client
	$connexion=getConnect();
	$requete="SELECT montant FROM (intervention NATURAL JOIN typeintervention) WHERE code='$code'"; 
	$resultat=$connexion->query($requete);
	$resultat->setFetchMode(PDO::FETCH_OBJ);
	$montantIntervention=$resultat->fetch();
	$resultat->closeCursor();
	return $montantIntervention;
}

function nomMecas(){
    $connexion=getConnect();
    $requete="SELECT nomEmp FROM employes WHERE categorie='Mecanicien'";
    $resultat=$connexion->query($requete);
    $resultat->setFetchMode(PDO::FETCH_OBJ);
    return $resultat;
}

function trouverEmpNom($nom){
        $connexion=getConnect();
        $requete="SELECT * FROM employes WHERE nomEmp='$nom'";
        $resultat=$connexion->query($requete);
        $resultat->setFetchMode(PDO::FETCH_OBJ);
        $res=$resultat->fetch();
        return $res;
    }

function lesIntersMeca($Meca,$thismonday,$nextmonday){
        $connexion=getConnect();
        $nomMeca=$Meca->nomEmp;
        $requete="SELECT * FROM intervention NATURAL JOIN client NATURAL JOIN typeintervention WHERE ('$thismonday'<=dateIntervention AND dateIntervention<'$nextmonday' AND nomMeca='$nomMeca') ORDER BY heureIntervention,dateIntervention";
        $resultat=$connexion->query($requete);
        $resultat->setFetchMode(PDO::FETCH_OBJ);
        return $resultat;
    }


    function lesFormMeca($Meca,$thismonday,$nextmonday){
        $connexion=getConnect();
        $nomMeca=$Meca->nomEmp;
        $requete="SELECT * FROM formation WHERE ('$thismonday'<=dateFormation AND dateFormation<'$nextmonday' AND nomEmp='$nomMeca') ORDER BY heureFormation,dateFormation";
        $resultat=$connexion->query($requete);
        $resultat->setFetchMode(PDO::FETCH_OBJ);
        return $resultat;
    }

function verificationUser(){
        $connexion=getConnect();
        $requete="SELECT * from user";
        $resultat=$connexion->query($requete);
        $resultat->setFetchMode(PDO::FETCH_OBJ);
        $res=$resultat->fetch();
        return $res;
    }

function ajouterIntervention($date_rdv,$heure_rdv,$meca_rdv,$clientID,$typeInter) {
        $connexion=getConnect();
        $requete="INSERT INTO intervention VALUES('0','$typeInter','$date_rdv','$heure_rdv','$meca_rdv','$clientID','En attente de paiement')";
        $connexion->query($requete);
    }

function getListeClients() { //Plus utilisée pour l'instant
        $connexion=getConnect();
        $requete="SELECT * FROM client";
        $resultat=$connexion->query($requete);
        $resultat->setFetchMode(PDO::FETCH_OBJ);
        return $resultat;
    }

function lesInterventions(){
        $connexion=getConnect();
        $requete="SELECT * FROM typeintervention";
        $resultat=$connexion->query($requete);
        $resultat->setFetchMode(PDO::FETCH_OBJ);
        return $resultat;
    }

function trouverInter($nom){
        $connexion=getConnect();
        $requete="SELECT * FROM typeintervention WHERE nomTI='$nom'";
        $resultat=$connexion->query($requete);
        $resultat->setFetchMode(PDO::FETCH_OBJ);
        return $resultat;
    }


	
