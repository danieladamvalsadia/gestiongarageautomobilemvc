<?php

require_once('modele/functions.php');

//VUE

    


	function affichageDirecteur() {
		$entete=
			'<form method="post" action="site.php">
                    <div class"interEmp">
					   <input type="submit" name="intervention" value="Gestion des interventions" /> 
                    </div>
                    <div class="gestEmp">
					   <input type="submit" name="employe" value="Gestion des employés" />
                    </div>
                    <div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
			</form>';
		$page='<p>Bienvenue</p>';
		require_once('gabarit.php');
	}
	
    function affichageEmploye($employes, $directeur){
		$entete=
			'<form method="post" action="site.php">
                    <div class"interEmp">
					   <input type="submit" name="intervention" value="Gestion des interventions" /> 
                    </div>
                    <div class="gestEmp">
					   <input type="submit" name="employe" value="Gestion des employés" />
                    </div>
                    <div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
			</form>';
		
		$page=
			//CREER PROFIL EMPLOYE
            '<div class="gestion"><fieldset>
                <div class="creerEmp">
                    <form method="post" action="site.php">
                        <fieldset>
                            <legend>
                                Créer employé 
                            </legend>
                            <p><label>Nom : </label><input type="text"  maxlength="15" required name="nomEmp" /></p>
                            <p><label>Identifiant : </label><input type="text" maxlength="15" required name="loginEmp" /></p>
                            <p><label>Mot de passe : </label><input type="password" maxlength="15" required name="mdpEmp" /></p>
                            <p><label>Catégorie : </label>
                                <select name="catEmp">           
                                    <option value="Agent" selected >Agent</option>          
                                    <option value="Mécanicien">Mécanicien</option>           
                                    <option value="Directeur">Directeur</option>                 
                                </select>   
                            </p>
                            <input type="submit" name="creerEmp" value="Créer" />
                        </fieldset>
                    </form>
                <div>';
			
			
		$page.=
			//MODIFIER PROFIL EMPLOYE
            '<div class="modifEmp">
                <form method="post" action="site.php">
				    <fieldset>
					   <legend>Modifier employé </legend>';
		if(!empty($employes) && sizeof($employes)>1){
			$page.='<p><label>Sélectionner un employé : </label>
				
				<select name="modifierS">';
				foreach($employes as $ligne){	
                    if($ligne->nomEmp != $directeur[0]->nomEmp) {
                        $page.='<option value="'.$ligne->nomEmp.'"> '.$ligne->categorie.' : '.$ligne->nomEmp.' </option>';
                    }
				}
				$page.='</select>	
						<p>
							<input type="submit" value="Modifier" name="modifEmp"/>
						</p>
						
						</p></fieldset></form><div>';
		}else{
			$page.='<p>Aucun employé à afficher</p></fieldset></form><div>';		 
		}
		
		$page.=
			//SUPPRIMER PROFIL EMPLOYE
			'<div class="suppEmp"><fieldset><legend>Supprimer employé</legend><form action="site.php" method="post">';
	
		if(!empty($employes) && sizeof($employes)>1){
                    $page.='<table>
                                <tr>
                                    <td> Choix </td>
                                    <td> Nom </td>
                                    <td> Catégorie </td>
                                    <td> Identifiant </td>
                                    <td> Mot de passe </td>
                                </tr>';
			foreach($employes as $ligne){
                if($ligne->nomEmp != $directeur[0]->nomEmp) {
				    $page.='<tr>
                                <td><input type="checkbox" value="'.$ligne->nomEmp.'" name="check[]" /></td> 
                                <td> '.$ligne->nomEmp.' </td>
                                <td> '.$ligne->categorie.' </td>
                                <td> '.$ligne->login.' </td>
                                <td> '.$ligne->mdp.' </td>
                            </tr>';
                }
			}
            $page.='</table>';
			$page.='<p><input type="submit" value="Supprimer" name="supprimerEmploye"/></p></form></fieldset>';
			
		}else{
			$page.='<p>Aucun employé à afficher</p>';		 
		}
		$page.='</fieldset></form><div>';
		
		require_once('gabarit.php');
	}
	function affichageIntervention($interventions) {
        $entete=
			'<form method="post" action="site.php">
                    <div class"interEmp">
					   <input type="submit" name="intervention" value="Gestion des interventions" /> 
                    </div>
                    <div class="gestEmp">
					   <input type="submit" name="employe" value="Gestion des employés" />
                    </div>
                    <div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
			</form>';
        
        $page=
            // CREER INTERVENTION
            '<div class="gestion"><fieldset><div class="creerEmp">
                <form method="post" action="site.php">
                    <fieldset>
                        <legend>Créer intervention</legend>
                            <p>
                                <label>Intervention : </label>
                                <input type="text" name="nomTI" maxlength="25" required/>
                            </p>
                            <p>
                                <label> Montant : </label>
                                <input type="number" min="0" max="9999" step="1" maxlength="4" name="montant" required> €

                            </p>
                            <p>
                                <label>Liste des pièces requises : </label>
                            </p>
                            <p>
                                <textarea name="listePiece" required></textarea>
                            </p>

                            <p>
                                <input type="submit" name="creerIntervention" value="Créer"/>
                            </p>
                    </fieldset>
                </form>';
            
        $page.= 
            // MODIFIER INTERVENTION
            '<form method="post" action="site.php">
                <fieldset>
                    <legend>Modifier intervention</legend>';
        if(!empty($interventions)){
			$page.='<p><label>Sélectionner une intervention : </label>
				
				<select name="nomTI">';
				foreach($interventions as $ligne){	
					$page.='<option value="'.$ligne->nomTI.'">'.$ligne->nomTI.'</option>';
							
				}
				$page.='</select>	
						<p>
							<input type="submit" value="Modifier" name="modifTI"/>
						</p>
						
						</p></fieldset></form>';
		}else{
			$page.='<p>Aucune intervention à afficher</p></fieldset></form>';		 
		}  
        
        $page.=
            // SUPPRIMER INTERVENTION
            '<form method="post" action="site.php">
                <fieldset>
                    <legend>Supprimer intervention</legend>';
        if(!empty($interventions)){
            $page.= '<table>
                        <tr>
                            <td> Choix </td>
                            <td> Intervention </td>
                            <td> Montant </td>
                            <td> Liste des pièces requises </td>
                        </tr>';
			foreach($interventions as $ligne){
				$page.='<tr>
                            <td> <input type="checkbox" value="'.$ligne->nomTI.'" name="check[]" /> </td>
                            <td> '.$ligne->nomTI.' </td>
                            <td> '.$ligne->montant. '€</p>
                            <td> '.$ligne->listePiece. '</p>';	
			}
			$page.='</table><p><input type="submit" value="Supprimer" name="supprimerIntervention"/></p></form></fieldset>';
			
		}else{
			$page.='<p>Aucune intervention à afficher</p>';		 
		}
		$page.='</fieldset></form></fieldset>';
        require_once('gabarit.php');
        
    }
	
    function affichageModifEmp($loginEmp, $mdpEmp, $catEmp, $nomEmp) {
		$entete=
			'<form method="post" action="site.php">
                    <div class"interEmp">
					   <input type="submit" name="intervention" value="Gestion des interventions" /> 
                    </div>
                    <div class="gestEmp">
					   <input type="submit" name="employe" value="Gestion des employés" />
                    </div>
                    <div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
			</form>';
		
		$page=
			'<form action="site.php" method="post"><fieldset>
				<legend>
					Modifier employé : '.$catEmp.' - '.$nomEmp.' 
				</legend>
							<p>
								
								<input type="text" hidden name="nomEmp" value="'.$nomEmp.'"/>
								
							</p>
							<p>
								<label>Identifiant : </label>
								<input type="text" name="loginEmp" maxlength="15" required value="'.$loginEmp.'"/>
								
							</p>
									
							<p>
								<label>Mot de passe : </label>
								<input type="password" name="mdpEmp" maxlength="15" required value="'.$mdpEmp.'"/>
							</p>
							
							
								<p><label>Catégorie : </label>
						<select name="catEmp">
           
							<option value="Agent" selected >Agent</option>          
							<option value="Mécanicien">Mécanicien</option>           
							<option value="Directeur">Directeur</option>                 
						</select>   
					</p>
							<p>
								<input type="submit" name="modifierM" value="Enregistrer"/>
							</p>
						
					
			</fieldset></form>';
		
		require_once('gabarit.php');
	}
    function affichageModifTI($montant, $listePiece, $nomTI) {
		$entete=
			'<form method="post" action="site.php">
                    <div class"interEmp">
					   <input type="submit" name="intervention" value="Gestion des interventions" /> 
                    </div>
                    <div class="gestEmp">
					   <input type="submit" name="employe" value="Gestion des employés" />
                    </div>
                    <div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
			</form>';
		
		$page=
			'<form action="site.php" method="post"><fieldset>
				<legend>
					Modifier intervention : '.$nomTI.' 
				</legend>
							<p>
								
								<input type="text" hidden readonly name="nomTI" value="'.$nomTI.'"/>
								
							</p>
                            <p>
							 <label> Montant : </label>
                            <input type="number" min="0" max="9999" step="1" maxlength="4" name="montant" required> €
									
							<p>
                           
								<label>Liste des pièces requises : </label>
                            </p>
                            <p>
                                <textarea name="listePiece">'.$listePiece.'</textarea> 
                            </p>
                                
                                
							<p>
								<input type="submit" name="modifierTI" value="Enregistrer"/>
							</p>
						
					
			</fieldset></form></fieldset><div>';
		
		require_once('gabarit.php');
	}
    
    function affichageConnexion() {
        $entete=
			'<form method="post" action="site.php">
				<p>Bienvenue sur KDfix.                       
			</form>';
		
        $page=
            '<form method="post" action="site.php">
				<fieldset>
                    
                        <p class="p1">
                            <label>Identifiant : </label>
                        </p>
                    
                        <p>
                            <input type="text" name="login" maxlength="15" required/>
                        </p>
                        <p class="p1">
                            <label>Mot de passe : </label>
                        </p>
                        <p>
                            <input type="password" name="mdp" maxlength="15" required/>
                        </p>   
                        
                        <p>
                            <input type="submit" name="connexion" value="Se connecter"/>
                        </p>
                </fieldset>
            </form>';
		
		require_once('gabarit.php');
    }
    function afficherErreurDirecteur($erreur){
        $entete=
			'<form method="post" action="site.php">
                    <div class"interEmp">
					   <input type="submit" name="intervention" value="Gestion des interventions" /> 
                    </div>
                    <div class="gestEmp">
					   <input type="submit" name="employe" value="Gestion des employés" />
                    </div>
                    <div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
			</form>';
		
        $page=
			'<p>'.
				$erreur.
			'</p>
			<p>
				<a href="site.php"/> Retour accueil </a>
			</p>';
        require_once('gabarit.php');
	
	}

    function afficherErreurConnexion(){
         $entete=
			'<form method="post" action="site.php">
				<p>Bienvenue sur KDfix.                       
			</form>';
		$page=
            '<form method="post" action="site.php">
				<fieldset>
                    
                        <p class="p1">
                            <label>Identifiant : </label>
                        </p>
                    
                        <p>
                            <input type="text" name="login" maxlength="15" required/>
                        </p>
                        <p class="p1">
                            <label>Mot de passe : </label>
                        </p>
                        <p>
                            <input type="password" name="mdp" maxlength="15" required/>
                        </p>   
                        
                        <p>
                            <input type="submit" name="connexion" value="Se connecter"/>
                        </p>
                        <p>
                            Identifiant ou mot de passe incorrect
                        </p>
                </fieldset>
            </form>';
        
        require_once('gabarit.php');
	
	}

function affichageAgent(){
	$entete='<form action="site.php" method="post">
	<div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
	<p><input type="submit" value="Gestion client" name="gestionCli"/></p>
	<p><input type="submit" name="rechercherIDClient" value="Rechercher l\'ID d\'un client"/></p>
	<p><input type="submit" value="Synthèse client" name="syntheseCli"/></p>
	<p><input type="submit" value="Gestion financière" name="gestionFin"/></p>
	<p><input type="submit" value="Gestion Rendez-vous" name="gestionRdv"/></p>
	</form>';
	
	$page='Bienvenue';
	 
	require_once('gabarit.php');  
}



function affichageErreurAgent($msg){
		$entete='<form action="site.php" method="post">
	<div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
	<p><input type="submit" value="Gestion client" name="gestionCli"/></p>
	<p><input type="submit" name="rechercherIDClient" value="Rechercher l\'ID d\'un client"/></p>
	<p><input type="submit" value="Synthèse client" name="syntheseCli"/></p>
	<p><input type="submit" value="Gestion financière" name="gestionFin"/></p>
	<p><input type="submit" value="Gestion Rendez-vous" name="gestionRdv"/></p>
	</form>';

	$page='<p>'.$msg.' <a href="site.php"/> Revenir au site </a> </p>';
	require_once('gabarit.php');  
}



//GESTION CLIENT
function affichageGestionCli($client){
		$entete='<form action="site.php" method="post">
	<div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
	<p><input type="submit" value="Gestion client" name="gestionCli"/></p>
	<p><input type="submit" name="rechercherIDClient" value="Rechercher l\'ID d\'un client"/></p>
	<p><input type="submit" value="Synthèse client" name="syntheseCli"/></p>
	<p><input type="submit" value="Gestion financière" name="gestionFin"/></p>
	<p><input type="submit" value="Gestion Rendez-vous" name="gestionRdv"/></p>
	</form>';
	
	//afficher/supprimer client
	$page='<fieldset><legend>Clients</legend><form action="site.php" method="post">
        <table>
            <tr>
                <td>Identifiant</td>
                <td>Nom</td>
                <td>Prenom</td>
                <td>Date de naissance</td>
                <td>Adresse postale</td>
                <td>Numéro de téléphone</td>
                <td>Email</td>
                <td>Montant maximum</td>
            </tr>';
	
	if(!empty($client)){
		
		foreach($client as $ligne){
			$page.='
                <tr>
                    <td><input type="checkbox" value="'.$ligne->idClient.'" name="check[]" /></td>
			         <td>'.$ligne->nom.'</td>
                     <td>'.$ligne->prenom.'</td>
                     <td>'.$ligne->date.'</td>
                     <td>'.$ligne->adresse.'</td>
                     <td>(+33)'.$ligne->num.'</td>
                     <td>'.$ligne->mail.'</td>
                     <td>'.$ligne->montantMax.'€ </td>
                </tr>';	
		}
		$page.='<table><p><input type="submit" value="Supprimer" name="supprimer"/></p></form></fieldset>';
		
	}else{
		$page.='<p>Aucun client à afficher</p>';		 
	}
	
	$page.='</fieldset></form>';
	
	
	//ajouter client
	$page.='<form action="site.php" method="post">
	<fieldset><legend>Ajouter client</legend><form action="site.php" method="post">
	<p><label for="Nom"> Nom </label><input type="varchar" name="nom" placeholder="15 caractères max" maxlength="15" required/></p>
	<p><label for="Prénom"> Prénom </label><input type="varchar" name="prenom" maxlength="15" placeholder="15 caractères maximum" required /></p>
	<p><label for="Date de naissance"> Date de naissance </label><input type="date" name="date" placeholder="aaaa-mm-jj" required/></p>
	<p><label for="Adresse"> Adresse </label><input type="text" name="adresse" required/></p>	
	<p><label for="Numéro"> Numéro de téléphone </label><input type="number" placeholder="9 chiffres maximum" name="num" min="100000000" max="999999999" required/></p>     
	<p><label for="Adresse mail"> Adresse mail </label><input type="text" name="mail" required/></p>
	<p><label for="Montant différé maximum"> Montant différé maximum </label><input type="number" placeholder="4 chiffres maximum" max="9999" min="0" name="montantMax" required/></p>
	<p><input type="submit" value="Ajouter client" name="ajouterCli"/></p>
	</fieldset></form>';
	//taille max de num=9 car premier chiffre tjr=0
	
	
	
	//modifier client
	$page.='<form action="site.php" method="post">
	<fieldset><legend>Modifier client</legend><form action="site.php" method="post">';
	if(!empty($client)){
	$page.='<p>Sélectionner un client à modifier :
				<select name="modifier">';
				foreach($client as $ligne){	
					$page.='<option value="'.$ligne->idClient.'"> Client n°'.$ligne->idClient.' '.$ligne->nom.' '.$ligne->prenom.' </option>';
				}
			$page.='</select>
			<p><input type="submit" value="Modifier client" name="modifierCli"/></p>
			</p>';
	}else{
		$page.='<p>Aucun client à afficher</p>';		 
	}

		$page.='</fieldset></form>';
	
	require_once('gabarit.php'); 
}
//page de modification de client
function affichageModifCli($nom,$prenom,$date,$adresse,$num,$mail,$montantMax,$idClient){
		$entete='<form action="site.php" method="post">
	<div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
	<p><input type="submit" value="Gestion client" name="gestionCli"/></p>
	<p><input type="submit" name="rechercherIDClient" value="Rechercher l\'ID d\'un client"/></p>
	<p><input type="submit" value="Synthèse client" name="syntheseCli"/></p>
	<p><input type="submit" value="Gestion financière" name="gestionFin"/></p>
	<p><input type="submit" value="Gestion Rendez-vous" name="gestionRdv"/></p>
	</form>';
		$page=
			'<form action="site.php" method="post"><fieldset>
				<legend>
					Modifier le client n° '.$idClient.' 
				</legend>
							<p>
								<input type="text" name="idClient" hidden value="'.$idClient.'"/>
							</p>
							<p>
								<label>Nom </label>
								<input type="text"  name="nom" maxlength="15" value="'.$nom.'"/>
							</p>
									
							<p>
								<label>Prénom </label>
								<input type="text" name="prenom" maxlength="15" value="'.$prenom.'"/>
							</p>
							
							<p>
								<label>Date de naissance </label>
								<input type="date" name="date" value="'.$date.'"/>
							</p>
							
							<p>
								<label>Adresse </label>
								<input type="text" name="adresse" value="'.$adresse.'"/>
							</p>
							
							<p>
								<label>Numéro de téléphone </label>
								<input type="number" name="num" min="100000000" max="999999999" value="'.$num.'"/>
							</p>
							<p>
								<label>Adresse mail </label>
								<input type="text" name="mail" value="'.$mail.'"/>
							</p>
							<p>
								<label>Montant maximum différé </label>
								<input type="number" name="montantMax" max="9999" min="0" value="'.$montantMax.'"/>
							</p>
							
							<p>
								<input type="submit" name="modifierClient" value="Modifier"/>
							</p>
			</fieldset></form>';
		require_once('gabarit.php');
	}
	
	
	
	
	
	
//RECHERCHER CLIENT

function affichageRechercherIDCli($client){
		$entete='<form action="site.php" method="post">
	<div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
	<p><input type="submit" value="Gestion client" name="gestionCli"/></p>
	<p><input type="submit" name="rechercherIDClient" value="Rechercher l\'ID d\'un client"/></p>
	<p><input type="submit" value="Synthèse client" name="syntheseCli"/></p>
	<p><input type="submit" value="Gestion financière" name="gestionFin"/></p>
	<p><input type="submit" value="Gestion Rendez-vous" name="gestionRdv"/></p>
	</form>';
	

	$page='<form action="site.php" method="post">
	<fieldset><legend>Rechercher l\'ID d\'un client</legend><form action="site.php" method="post">
	<p><label for="Nom"> Nom </label><input type="varchar" name="nom" placeholder="15 caractères max" maxlength="15" required/></p>
	<p><label for="Prénom"> Prénom </label><input type="varchar" name="prenom" maxlength="15" placeholder="15 caractères maximum" required /></p>
	<p><label for="Date de naissance"> Date de naissance </label><input type="date" name="date" placeholder="aaaa-mm-jj" required/></p>
		<p>
		<input type="submit" name="rechercherClient" value="Rechercher client"/>
		</p>	
	</fieldset></form>';
	if($client!="1"){
		
		if(($client==false)){//client mal saisi
			$page.='<fieldset><legend>Dernière recherche d\'ID </legend>';

			$page.='<p>Aucun client correspondant.</p>';

			$page.='</fieldset>';
			}
			
		if($client!=false){ 
			$page.='<fieldset><legend>Dernière recherche d\'ID </legend>';

			$page.='<p>L\'ID correspondant à '.$client->nom.' '.$client->prenom.' est '.$client->idClient.'.</p>';

			$page.='</fieldset>';
		}
	}
		require_once('gabarit.php');
	}
	
	
	
	
//SYNTHESE CLIENT
function affichageSyntheseCli($clients,$client,$intervention){
		$entete='<form action="site.php" method="post">
	<div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
	<p><input type="submit" value="Gestion client" name="gestionCli"/></p>
	<p><input type="submit" name="rechercherIDClient" value="Rechercher l\'ID d\'un client"/></p>
	<p><input type="submit" value="Synthèse client" name="syntheseCli"/></p>
	<p><input type="submit" value="Gestion financière" name="gestionFin"/></p>
	<p><input type="submit" value="Gestion Rendez-vous" name="gestionRdv"/></p>
	</form>';
	$page='<form action="site.php" method="post">
	<fieldset><legend>Synthèse client</legend><form action="site.php" method="post">';
	
	if(!empty($clients)){  //saisie client = on ne fait pas de select car le sujet demande une saisie et la fct "rechercherIDClient" ne servirait à rien
	$page.='	<p>
				<label>Saisissez un client à consulter : </label>
				<input type="number" name="idClient" min="1"/>
				</p>
				<p>
				<input type="submit" value="Voir la synthèse" name="rechercherSyntheseCli"/></p>
				</p>';
	}else{
		$page.='<p>Aucun client à rechercher.</p>';		 
	}
	
	
	$page.='</fieldset></form>';
	
	if($client!='1'){ 
		if($client!=false){ 
			$page.='<fieldset><legend>Client n°'.$client->idClient.' </legend>
			<fieldset><legend>Informations personnelles </legend> 
	<p><label for="Nom"> Nom </label><input type="varchar" name="nom" value="'.$client->nom.'" readonly/></p>
	<p><label for="Prénom"> Prénom </label><input type="varchar" name="prenom" value="'.$client->prenom.'" readonly /></p>
	<p><label for="Date de naissance"> Date de naissance </label><input type="date" name="date" value="'.$client->date.'" readonly/></p>
	<p><label for="Adresse"> Adresse </label><input type="text" name="adresse" value="'.$client->adresse.'" readonly/></p>	
	<p><label for="Numéro"> Numéro de téléphone </label><input type="number" value="'.$client->num.'" readonly/></p>     
	<p><label for="Adresse mail"> Adresse mail </label><input type="text" name="mail" value="'.$client->mail.'" readonly/></p>
	<p><label for="Montant différé maximum"> Montant différé maximum </label><input type="number" value="'.$client->montantMax.'" name="montantMax" readonly/></p>
	</fieldset>'; //info perso client
	
	
	$page.='<fieldset><legend>Intervention réalisées </legend>';
	if(!empty($intervention)){
			$page.='<table>
                <tr>
                    <td>Intervention</td>
                    <td>Date intervention</td>
                    <td>Mecanicien</td>
                    <td>Montant</td>
                    <td>Etat</td>
                </tr>';		
        
		foreach($intervention as $ligne){
			$page.='
                <tr>
                    <td>'.$ligne->nomTI.'</td>
                    <td>'.$ligne->dateIntervention.'h </td>
                    <td>'.$ligne->nomMeca.' </td>
                    <td>'.$ligne->montant.' </td>
                    <td>'.$ligne->etat.'</td>
                </tr>';
		}
		$page.='</table>';
	}else{
		$page.='<p>Aucune intervention réalisée.</p>';		 
	}
	$page.='</fieldset></fieldset>';
	
	
		
	}else{
		$page.='<p> Aucun client correspondant.</p>';

	}
	}
			require_once('gabarit.php');
}


	
//GESTION FINANCIERE
function affichageGestionFin($clients,$client,$interventionsDifférées,$interventionAPayer,$montantMax){
			$entete='<form action="site.php" method="post">
	<div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
	<p><input type="submit" value="Gestion client" name="gestionCli"/></p>
	<p><input type="submit" name="rechercherIDClient" value="Rechercher l\'ID d\'un client"/></p>
	<p><input type="submit" value="Synthèse client" name="syntheseCli"/></p>
	<p><input type="submit" value="Gestion financière" name="gestionFin"/></p>
	<p><input type="submit" value="Gestion Rendez-vous" name="gestionRdv"/></p>
	</form>';
	
	$page='<form action="site.php" method="post">
	<fieldset><legend>Finances client</legend><form action="site.php" method="post">';
	
	//selection client
	if(!empty($clients)){ 
	$page.='<p>Sélectionner un client à consulter :
				<select name="idClient">';
				foreach($clients as $ligne){	
					$page.='<option value="'.$ligne->idClient.'"> Client n°'.$ligne->idClient.' '.$ligne->nom.' '.$ligne->prenom.' </option>';
				}
			$page.='</select>
			<p><input type="submit" value="Voir les finances" name="rechercherFinanceCli"/></p>
			</p>';
	}else{
		$page.='<p>Aucun client à afficher.</p>';		 
	}
	$page.='</fieldset></form>';
	
	
	
	//affichage interventions
	if($client !=null){
			$page.='<form action="site.php" method="post"><legend>Finances de '.$client->nom.' '.$client->prenom.' </legend><fieldset>';
	if($interventionsDifférées!=null || $interventionAPayer!=null){


	
		//dernière à payer
		if($interventionAPayer!=null){
			$page.='<form action="site.php" method="post"><fieldset><legend>Dernière intervention en attente de payement </legend>
					<p>
					<p><input type="int" name="idClient" value="'.$client->idClient.'" hidden/></p>
					<p><input type="checkbox" value="'.$interventionAPayer->code.'" name="check[]"/>'.$interventionAPayer->nomTI.' '.$interventionAPayer->dateIntervention.'  '.$interventionAPayer->nomMeca.' '.$interventionAPayer->montant.'</p>
					<p>Crédit restant : '.$montantMax->montantMax.'€ </p>
					<input type="submit" value="Régler en différé" name="payementDifféré"/> 
					<input type="submit" value="Régler en caisse" name="payer"/>
					</p>					
					</fieldset></form>';			
			
		}else{
			$page.='<fieldset><legend>Dernière intervention en attente de payement </legend>
					<p>Aucune intervention à régler</p>
					<p>Crédit restant : '.$montantMax->montantMax.'€ </p>
					</fieldset>';
		}

			//interventions en différé TERMINE
		if($interventionsDifférées!=null){
			$page.='<form action="site.php" method="post"><fieldset><legend>Interventions avancées en différé </legend>
			<p><input type="int" name="idClient" value="'.$client->idClient.'" hidden/></p>
                <table>
                    <tr>
                        <td>Choix</td>
                        <td>Intervention</td>
                        <td>Date intervention</td>
                        <td>Mecanicien</td>
                        <td>Montant</td>
                    </tr>';
				
            foreach($interventionsDifférées as $ligne){ //Tableau
					$page.='
                        <tr>
                            <td><input type="checkbox" value="'.$ligne->code.'" name="check[]"/></td>
                            <td>'.$ligne->nomTI.'</td>
                            <td>'.$ligne->dateIntervention.'</td>
                            <td>'.$ligne->nomMeca.'</td>
                            <td>'.$ligne->montant.'</td>
                        <tr>';
						
				}
		$page.='</table><p><input type="submit" value="Rembourser" name="rembourser"/></p></fieldset></form>';
		}else{
			$page.='<fieldset><legend>Interventions avancées en différé </legend>
					<p>Aucune intervention à régler</p>
					</fieldset>';
		}
		
		
		
		$page.='</fieldset></form>';
		
		
		
	}else{
		$page.='<fieldset><legend>Intervention à régler </legend>
		<p>Aucune intervention à régler</p>
				</fieldset>';		 
	}
	}
	$page.='</fieldset>';
	
	
		
	
		require_once('gabarit.php');
	}

function affAjouterIntervention($date_rdv,$heure_rdv,$meca_rdv,$user,$listeTypeInter,$listeClient) {
        $entete='<form action="site.php" method="post">
                <div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
                <p><input type="submit" value="Gestion client" name="gestionCli"/></p>
                <p><input type="submit" name="rechercherIDClient" value="Rechercher l\'ID d\'un client"/></p>
                <p><input type="submit" value="Synthèse client" name="syntheseCli"/></p>
                <p><input type="submit" value="Gestion financière" name="gestionFin"/></p>
                <p><input type="submit" value="Gestion Rendez-vous" name="gestionRdv"/></p>
                </form>';
        $page='<br><form method="post" action="site.php">
                        <fieldset>
                        <legend>Ajouter une intervention pour le mecanicien '.$meca_rdv.'</legend>
                        <p>Date de l\'intervention : '.$date_rdv.'</p>
                        <p>Heure de l\'intervention : '.$heure_rdv.'h00</p>
                        ';
        $ligne=$listeClient->fetch();
        if(!empty($ligne) && !is_bool($ligne)){
            $page.='<p>
                        <label name="clientID">Client&nbsp;: </label>
                        <select name="clientID" id="clientID">';
            do{
                $page.='<option value="'.$ligne->idClient.'">'.$ligne->nom.' '.$ligne->prenom.'</option>';
            }while($ligne=$listeClient->fetch());
            $page.='</select>
            </p>';
        }else{
            $pasclient=false;
            $page.= '<p>Aucun client inscrit dans la base de données !</p>';
        }
        $page.='    </select>
                </p>';
        $ligne1=$listeTypeInter->fetch();
        if(!empty($ligne1) && !is_bool($ligne1)){
            $page.='    </select>
                </p>
                <p>
                    <label name="listeTypeInter">Type d\'intervention&nbsp;: </label>
                    <select name="listeTypeInter" id="listeTypeInter">';
            do{
                $page.='<option value="'.$ligne1->nomTI.'">'.$ligne1->nomTI.'</option>';
            }while($ligne1=$listeTypeInter->fetch());
            $page.='</select>
            </p>';
        }else{
            $pastypeinter=false;
            $page.= '<p>Aucun type d\'intervention inscrit dans la base de données !</p>';
        }
        if (!isset($pastypeinter) && !isset($pasclient)) {
            $page.='
                    <p>
                        Voulez-vous ajouter cette intervention ?
                    </p>
                    <p>
                        <input type="hidden" name="heure_rdv" value="'.$heure_rdv.'">
                        <input type="hidden" name="date_rdv" value="'.$date_rdv.'">
                        <input type="hidden" name="mecas" value="'.$meca_rdv.'">
                        <input type="hidden" name="nomMeca" value="'.$meca_rdv.'">
                        <input type="submit" name="prendre_rdv1" value="Oui" formmethod="post">
                        <input type="submit" name="voir_plan_agent" value="Non" formmethod="post">
                    </p>
                    </fieldset>
                </form>';
        } else {
            $page.='
                    <p>
                        Vous ne pouvez pas ajouter l\'intervention car les informations sont incomplètes !
                    </p>
                    <p>
                        <input type="hidden" name="mecas" value="'.$meca_rdv.'">
                        <input type="submit" name="gestionRdv" value="Retour" formmethod="post">
                    </p>
                    </fieldset>
                </form>';
        }
        require_once('gabarit.php');
    }

function planning_agent($interventions,$lundi,$user,$nomMecanicien,$formations) {
        $planning='<table>';
        $planning.='<tr>
                    <td></td>
                    <td>LUNDI</td>
                    <td>MARDI</td>
                    <td>MERCREDI</td>
                    <td>JEUDI</td>
                    <td>VENDREDI</td>
                    </tr>';
        $ligneforma=$formations->fetch();
        $ligne=$interventions->fetch();
        $mardi=demain($lundi);
        $mercredi=demain($mardi);
        $jeudi=demain($mercredi);
        $vendredi=demain($jeudi);
        $planning.='<tr>
                    <td></td>
                    <td>'.$lundi.'</td>
                    <td>'.$mardi.'</td>
                    <td>'.$mercredi.'</td>
                    <td>'.$jeudi.'</td>
                    <td>'.$vendredi.'</td>
                    </tr>';
        for ($heure=8; $heure < 18; $heure++) { 
            $planning.='<tr>';
            
            for ($jour=0; $jour < 6; $jour++) { 
                if ($jour==0) {
                    $heure1=$heure+1;
                    $planning.='<td>'.$heure.'h-'.$heure1.'h</td>';
                }else{
                    if(!empty($ligne)){
                        if ($heure==$ligne->heureIntervention && $jour==date( 'N', strtotime( $ligne->dateIntervention ) )) {
                            $planning.='<th>
                                            <form method="post" action="site.php">
                                                <input type="hidden" name="nomClient" value="'.$ligne->nom.'">
                                                <input type="hidden" name="prenomClient" value="'.$ligne->prenom.'">
                                                <input type="hidden" name="numClient" value="'.$ligne->num.'">
                                                <input type="hidden" name="mail" value="'.$ligne->mail.'">
                                                <input type="hidden" name="typeIntervention" value="'.$ligne->nomTI.'">
                                                <input type="hidden" name="pieces" value="'.$ligne->listePiece.'">
                                                <input type="hidden" name="heure" value="'.$heure.'">
                                                <input type="hidden" name="date" value="'.quelJour($lundi,$jour).'">
                                                <input type="hidden" name="mecas" value="'.$nomMecanicien.'">
                                                <input type="submit" name="voir_intervention_agent" value="Voir Intervention" formmethod="post">
                                            </form>
                                        </th>';
                            $ligne=$interventions->fetch();
                        }else if(!empty($ligneforma)){
                            if ($heure==$ligneforma->heureFormation && $jour==date( 'N', strtotime( $ligneforma->dateFormation ) )) {
                                $planning.='<th>Formation</th>';
                                $ligneforma=$formations->fetch();
                            }else{
                                $planning.='<th>
                                                <form method="post" action="site.php">
                                                    <input type="hidden" name="heure_rdv" value="'.$heure.'">
                                                    <input type="hidden" name="date_rdv" value="'.quelJour($lundi,$jour).'">
                                                    <input type="hidden" name="nomMeca_rdv" value="'.$nomMecanicien.'">
                                                    <input type="submit" name="prendre_rdv" value="Prendre RDV" formmethod="post">
                                                </form>
                                            </th>';
                            }
                        }else{
                            $planning.='<th>
                                            <form method="post" action="site.php">
                                                <input type="hidden" name="heure_rdv" value="'.$heure.'">
                                                <input type="hidden" name="date_rdv" value="'.quelJour($lundi,$jour).'">
                                                <input type="hidden" name="nomMeca_rdv" value="'.$nomMecanicien.'">
                                                <input type="submit" name="prendre_rdv" value="Prendre RDV" formmethod="post">
                                            </form>
                                        </th>';
                        }
                    }else if(!empty($ligneforma)){
                        if ($heure==$ligneforma->heureFormation && $jour==date( 'N', strtotime( $ligneforma->dateFormation ) )) {
                            $planning.='<th>Formation</th>';
                            $ligneforma=$formations->fetch();
                        }else{
                            $planning.='<th>
                                            <form method="post" action="site.php">
                                                <input type="hidden" name="heure_rdv" value="'.$heure.'">
                                                <input type="hidden" name="date_rdv" value="'.quelJour($lundi,$jour).'">
                                                <input type="hidden" name="nomMeca_rdv" value="'.$nomMecanicien.'">
                                                <input type="submit" name="prendre_rdv" value="Prendre RDV" formmethod="post">
                                            </form>
                                        </th>';
                        }
                    }else{
                        $planning.='<th>
                                        <form method="post" action="site.php">
                                            <input type="hidden" name="heure_rdv" value="'.$heure.'">
                                            <input type="hidden" name="date_rdv" value="'.quelJour($lundi,$jour).'">
                                            <input type="hidden" name="nomMeca_rdv" value="'.$nomMecanicien.'">
                                            <input type="submit" name="prendre_rdv" value="Prendre RDV" formmethod="post">
                                        </form>
                                    </th>';
                    }
                }
            }

            $planning.='</tr>';
        }

        $planning.='</table>';
        return $planning;
    }

function voirIntervention_agent($nomClient,$prenomClient,$numClient,$mailClient,$type_intervention,$pieces,$date,$heure,$meca){
         $entete='<form action="site.php" method="post">
                <div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
                <p><input type="submit" value="Gestion client" name="gestionCli"/></p>
                <p><input type="submit" name="rechercherIDClient" value="Rechercher l\'ID d\'un client"/></p>
                <p><input type="submit" value="Synthèse client" name="syntheseCli"/></p>
                <p><input type="submit" value="Gestion financière" name="gestionFin"/></p>
                <p><input type="submit" value="Gestion Rendez-vous" name="gestionRdv"/></p>
                </form>';
        
        $page='<br><p><strong>Voir Intervention</strong></p>
                <p>La date d\'intervention : '.$date.'<p>
                <p>L\'heure d\'intervention : '.$heure.'h00</p>
                <p>Nom du client : '.$nomClient.'</p>
                <p>Prenom du client : '.$prenomClient.'</p>
                <p>Numero du client : 0'.$numClient.'</p>
                <p>Mail du client : '.$mailClient.'</p>
                <p>Type de l\'intervention : '.$type_intervention.'</p>
                <p>Pièce(s) à joindre : '.$pieces.'</p>
                <form method="post" action="site.php">
                    <p>
                        <input type="hidden" name="nomMeca" value="'.$meca.'">
                        <input type="submit" name="voir_plan_agent" value="Retour" formmethod="post">
                    </p>
                </form>';
        
        require_once('gabarit.php');
    }

    function affLDMecas($listeMecas){
        $entete='<form action="site.php" method="post">
                <div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
                <p><input type="submit" value="Gestion client" name="gestionCli"/></p>
                <p><input type="submit" name="rechercherIDClient" value="Rechercher l\'ID d\'un client"/></p>
                <p><input type="submit" value="Synthèse client" name="syntheseCli"/></p>
                <p><input type="submit" value="Gestion financière" name="gestionFin"/></p>
                <p><input type="submit" value="Gestion Rendez-vous" name="gestionRdv"/></p>
                </form>';
        if(($l=$listeMecas->fetch())!=false){
            $page='<form method="post" action="site.php">
            <fieldset><legend>« Modifier une Intervention »</legend>
            <p>
                <label for="nomMeca">Nom des mecaniciens :</label>
                <select name="nomMeca" id="nomMeca">';
            do{
                $page.='<option value="'.$l->nomEmp.'">'.$l->nomEmp.' </option>';
            }while($l=$listeMecas->fetch());
            $page.='</select></p><p><input type="submit" name="voir_plan_agent" value="Voir Son Planning"/></p>';
            $page.='</fieldset></form>';
        }
        require_once('gabarit.php');
    }

    function affPlanningAgent($listeMecas,$thismonday,$user,$Meca,$interventions,$formations){
        $entete='<form action="site.php" method="post">
                <div class="deco">
                        <input type="submit" name="seDeconnecter" value="Se déconnecter" />
                    </div>
                <p><input type="submit" value="Gestion client" name="gestionCli"/></p>
                <p><input type="submit" name="rechercherIDClient" value="Rechercher l\'ID d\'un client"/></p>
                <p><input type="submit" value="Synthèse client" name="syntheseCli"/></p>
                <p><input type="submit" value="Gestion financière" name="gestionFin"/></p>
                <p><input type="submit" value="Gestion Rendez-vous" name="gestionRdv"/></p>
                </form>';
        if(($l=$listeMecas->fetch())!=false){
            $page='<form method="post" action="site.php">
            <fieldset><legend>« Modifier une Intervention »</legend>
            <p>
                <label for="nomMeca">Nom des mecaniciens :</label>
                <select name="nomMeca" id="nomMeca">';
            do{
                $page.='<option value="'.$l->nomEmp.'">'.$l->nomEmp.' </option>';
            }while($l=$listeMecas->fetch());
            $page.='</select></p><p><input type="submit" name="voir_plan_agent" value="Voir Son Planning"/></p>';
            $page.='</fieldset></form>';
        }
        $page.='<div id="buttonsSemaines"><form method="post" action="site.php">
                        <input type="hidden" name="lundi" value="'.$thismonday.'">
                        <input type="hidden" name="user" value="'.$user->nomEmp.'">
                        <input type="hidden" name="Mecanicien" value="'.$Meca->nomEmp.'">
                        <input type="submit" name="semaine_pre_agent" id="semaine_pre_agent" value="Semaine précédente">
                        <input type="submit" name="semaine_pro_agent" id="semaine_pro_agent" value="Semaine Prochaine">
                    </form></div><br>';
        $page.=planning_agent($interventions,$thismonday,$user->nomEmp,$Meca->nomEmp,$formations);
        require_once('gabarit.php');
    }