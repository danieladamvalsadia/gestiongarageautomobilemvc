-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  mar. 09 jan. 2018 à 02:33
-- Version du serveur :  10.1.28-MariaDB
-- Version de PHP :  7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `kdfix`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `idClient` int(11) NOT NULL,
  `nom` varchar(15) NOT NULL,
  `prenom` varchar(15) NOT NULL,
  `date` date NOT NULL,
  `adresse` text NOT NULL,
  `num` int(9) NOT NULL,
  `mail` text NOT NULL,
  `montantMax` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`idClient`, `nom`, `prenom`, `date`, `adresse`, `num`, `mail`, `montantMax`) VALUES
(1, 'C1', 'C1', '1970-01-25', 'adresse1', 659824807, 'daniel.adam1998@gmail.com', 5000),
(2, 'C2', 'C2', '1998-01-03', 'adresse2', 659824808, 'juliette.douare1997@gmail.com', 400),
(3, 'C3', 'C3', '1998-01-03', 'adresse3', 659824809, 'juliette.douare@etu.univ-orleans.fr', 1000),
(4, 'C4', 'C4', '1998-01-03', 'adresse4', 659824810, 'daniel.adam@etu.univ-orleans.fr', 2000);

-- --------------------------------------------------------

--
-- Structure de la table `employes`
--

CREATE TABLE `employes` (
  `nomEmp` varchar(15) NOT NULL,
  `login` varchar(15) NOT NULL,
  `mdp` varchar(15) NOT NULL,
  `categorie` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `employes`
--

INSERT INTO `employes` (`nomEmp`, `login`, `mdp`, `categorie`) VALUES
('A1', 'A1', 'mdp', 'Agent'),
('A2', 'A2', 'mdp', 'Agent'),
('D1', 'D1', 'mdp', 'Directeur'),
('D2', 'D2', 'mdp', 'Directeur'),
('Daniel', 'DADAM', 'lol', 'Directeur'),
('M1', 'M1', 'mdp', 'Mécanicien');

-- --------------------------------------------------------

--
-- Structure de la table `formation`
--

CREATE TABLE `formation` (
  `idFormation` int(15) NOT NULL,
  `dateFormation` date NOT NULL,
  `heureFormation` int(2) NOT NULL,
  `nomEmp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `intervention`
--

CREATE TABLE `intervention` (
  `code` int(12) NOT NULL,
  `nomTI` varchar(25) NOT NULL,
  `dateIntervention` date NOT NULL,
  `heureIntervention` int(2) NOT NULL,
  `nomMeca` varchar(15) NOT NULL,
  `idClient` int(12) NOT NULL,
  `etat` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `typeintervention`
--

CREATE TABLE `typeintervention` (
  `nomTI` varchar(25) NOT NULL,
  `montant` int(12) NOT NULL,
  `listePiece` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `typeintervention`
--

INSERT INTO `typeintervention` (`nomTI`, `montant`, `listePiece`) VALUES
('Pneu', 80, 'papier'),
('Rehausseur', 200, 'papier'),
('Révision', 90, 'papier'),
('Vidange', 50, 'papier');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `nomEmp` varchar(15) NOT NULL,
  `login` varchar(15) NOT NULL,
  `mdp` varchar(15) NOT NULL,
  `categorie` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`nomEmp`, `login`, `mdp`, `categorie`) VALUES
('D1', 'D1', 'mdp', 'Directeur');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`idClient`);

--
-- Index pour la table `employes`
--
ALTER TABLE `employes`
  ADD PRIMARY KEY (`nomEmp`,`login`);

--
-- Index pour la table `formation`
--
ALTER TABLE `formation`
  ADD PRIMARY KEY (`idFormation`),
  ADD KEY `FK_FORMATION-nomEmp` (`nomEmp`);

--
-- Index pour la table `intervention`
--
ALTER TABLE `intervention`
  ADD PRIMARY KEY (`code`),
  ADD KEY `FK_idClient` (`idClient`),
  ADD KEY `FK_nomMeca` (`nomMeca`),
  ADD KEY `FK_nomTI` (`nomTI`);

--
-- Index pour la table `typeintervention`
--
ALTER TABLE `typeintervention`
  ADD PRIMARY KEY (`nomTI`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`nomEmp`,`login`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `idClient` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `formation`
--
ALTER TABLE `formation`
  MODIFY `idFormation` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `intervention`
--
ALTER TABLE `intervention`
  MODIFY `code` int(12) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `formation`
--
ALTER TABLE `formation`
  ADD CONSTRAINT `FK_FORMATION-nomEmp` FOREIGN KEY (`nomEmp`) REFERENCES `employes` (`nomEmp`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `intervention`
--
ALTER TABLE `intervention`
  ADD CONSTRAINT `FK_idClient` FOREIGN KEY (`idClient`) REFERENCES `client` (`idClient`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_nomMeca` FOREIGN KEY (`nomMeca`) REFERENCES `employes` (`nomEmp`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_nomTI` FOREIGN KEY (`nomTI`) REFERENCES `typeintervention` (`nomTI`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
